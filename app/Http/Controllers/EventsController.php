<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReservePlaceRequest;
use Illuminate\Support\Facades\Http;

class EventsController extends Controller
{
    public function index(int $id)
    {
        $places = Http::withToken(config('services.token'))
            ->get(config('services.api_url') . "/events/{$id}/places")
            ->json()['response'];

        return view('events/index', compact('id', 'places'));
    }

    public function reserve(ReservePlaceRequest $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();

        $result = Http::withToken(config('services.token'))
            ->asForm()
            ->post(config('services.api_url') . "/events/{$id}/reserve", $data)
            ->json();

        if (isset($result['error'])) {
            return back()->with('error', $result['error']);
        }

        return back()->with('success', $result['response']['reservation_id']);
    }
}
