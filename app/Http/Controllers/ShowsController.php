<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

class ShowsController extends Controller
{
    public function index()
    {
        $shows = Http::withToken(config('services.token'))
            ->get(config('services.api_url') . '/shows')
            ->json()['response'];

        return view('shows/index', compact('shows'));
    }

    public function events(int $id)
    {
        $events = Http::withToken(config('services.token'))
            ->get(config('services.api_url') . "/shows/{$id}/events")
            ->json()['response'];

        return view('shows/events', compact('id', 'events'));
    }
}
