<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title ?? config('app.name') }}</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
    <nav class="font-sans flex flex-col text-center sm:flex-row sm:text-left sm:justify-between py-4 px-6 bg-white shadow sm:items-baseline w-full">
        <div class="mb-2 sm:mb-0">
            <a href="/" class="text-2xl no-underline text-grey-darkest hover:text-blue-dark">{{ config('app.name') }}</a>
        </div>
        <div>
            <span class="text-green-600">{{ $title }}</span>
        </div>
    </nav>

    {{ $slot }}

    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
