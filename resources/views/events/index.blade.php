<x-layout>
    <x-slot name="title">
        Event #{{ $id }} - Places {{ config('api.url') }}
    </x-slot>

    <div class="h-screen w-auto bg-gray-300">
        <h1 class="text-3xl font-medium tracking-wide text-gray-800 dark:text-white md:text-4xl text-center">Choose a place what you want</h1>

        <div class="canvas-holder">
            <canvas id="canvas1">
                Your browser does not support the canvas element.
            </canvas>
        </div>

        <div class="container h-auto mx-auto flex justify-center items-center p-2 md:p-0">
            <div class="border border-gray-300 p-6 grid grid-cols-1 gap-6 bg-white shadow-lg rounded-lg">
                <div class="grid">
                    <div class="grid grid-cols-1 gap-2 border border-gray-200 p-2 rounded">
                        <div class="flex border rounded bg-gray-300 items-center p-2">
                            <form method="post" action="{{ route('reserve', $id) }}" id="reservePlaceForm">
                                @csrf
                                <input name="name" type="text" placeholder="Enter your name..."
                                       class="bg-gray-300 max-w-full focus:outline-none text-gray-700"/>
                            </form>
                        </div>
                    </div>
                </div>
                @if (session('success'))
                    <div class="relative flex flex-col sm:flex-row sm:items-center bg-gray-200 shadow rounded-md py-5 pl-6 pr-8 sm:pr-6">
                        <div class="flex flex-row items-center border-b sm:border-b-0 w-full sm:w-auto pb-4 sm:pb-0">
                            <div class="text-green-500">
                                <svg
                                    viewBox="0 0 24 24"
                                    class="text-green-600 w-5 h-5 sm:w-5 sm:h-5 mr-3"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M12,0A12,12,0,1,0,24,12,12.014,12.014,0,0,0,12,0Zm6.927,8.2-6.845,9.289a1.011,1.011,0,0,1-1.43.188L5.764,13.769a1,1,0,1,1,1.25-1.562l4.076,3.261,6.227-8.451A1,1,0,1,1,18.927,8.2Z"
                                    ></path>
                                </svg>
                            </div>
                            <div class="text-sm font-medium ml-3 text-black">Success</div>
                        </div>
                        <div class="text-sm tracking-wide text-gray-500 mt-4 sm:mt-0 sm:ml-4">The selected places have been successfully reserved! Your reservation ID is <strong>{{ session('success') }}</strong></div>
                    </div>
                @endif

                @if (session('error'))
                    <div class="relative flex flex-col sm:flex-row sm:items-center bg-gray-200 shadow rounded-md py-5 pl-6 pr-8 sm:pr-6">
                        <div class="flex flex-row items-center border-b sm:border-b-0 w-full sm:w-auto pb-4 sm:pb-0">
                            <div class="text-green-500">
                                <svg
                                    viewBox="0 0 24 24"
                                    class="text-red-600 w-5 h-5 sm:w-5 sm:h-5 mr-3"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M11.983,0a12.206,12.206,0,0,0-8.51,3.653A11.8,11.8,0,0,0,0,12.207,11.779,11.779,0,0,0,11.8,24h.214A12.111,12.111,0,0,0,24,11.791h0A11.766,11.766,0,0,0,11.983,0ZM10.5,16.542a1.476,1.476,0,0,1,1.449-1.53h.027a1.527,1.527,0,0,1,1.523,1.47,1.475,1.475,0,0,1-1.449,1.53h-.027A1.529,1.529,0,0,1,10.5,16.542ZM11,12.5v-6a1,1,0,0,1,2,0v6a1,1,0,1,1-2,0Z"
                                    ></path>
                                </svg>
                            </div>
                            <div class="text-sm font-medium ml-3 text-black">Error</div>
                        </div>
                        <div class="text-sm tracking-wide text-gray-500 mt-4 sm:mt-0 sm:ml-4">{{ session('error') }}</div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="relative flex flex-col sm:flex-row sm:items-center bg-gray-200 shadow rounded-md py-5 pl-6 pr-8 sm:pr-6">
                        <div class="flex flex-row items-center border-b sm:border-b-0 w-full sm:w-auto pb-4 sm:pb-0">
                            <div class="text-green-500">
                                <svg
                                    viewBox="0 0 24 24"
                                    class="text-yellow-600 w-5 h-5 sm:w-5 sm:h-5 mr-3"
                                >
                                    <path
                                        fill="currentColor"
                                        d="M23.119,20,13.772,2.15h0a2,2,0,0,0-3.543,0L.881,20a2,2,0,0,0,1.772,2.928H21.347A2,2,0,0,0,23.119,20ZM11,8.423a1,1,0,0,1,2,0v6a1,1,0,1,1-2,0Zm1.05,11.51h-.028a1.528,1.528,0,0,1-1.522-1.47,1.476,1.476,0,0,1,1.448-1.53h.028A1.527,1.527,0,0,1,13.5,18.4,1.475,1.475,0,0,1,12.05,19.933Z"
                                    ></path>
                                </svg>
                            </div>
                            <div class="text-sm font-medium ml-3 text-black">Error</div>
                        </div>
                        <div class="text-sm tracking-wide text-gray-500 mt-4 sm:mt-0 sm:ml-4">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                <div class="flex justify-center">
                    <button form="reservePlaceForm" class="p-2 border rounded-md bg-gray-800 text-white" type="submit">Reserve</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/scheme-designer.min.js"></script>
    <script>
        let schemeData = [];

        @php
            foreach ($places as $place) {
                echo '
        schemeData.push({
            ObjectName: "Place",
            ObjectType: "Place",
            Width: ' . $place['width'] . ',
            Height: ' . $place['height'] . ',
            CX: ' . $place['y'] . ',
            CY: ' . $place['x'] . ',
            Seat: ' . $place['id'] . ',
            BackColor: "CC3366",
            Available: ' . ($place['is_available'] ? "true" : "false") . ',
        })';
            }
        @endphp

        let defaultLayer = new SchemeDesigner.Layer('default', {zIndex: 10});

        /**
         * Render place function
         * @param {SchemeObject} schemeObject
         * @param {Scheme} schemeDesigner
         * @param {View} view
         */
        let renderPlace = function (schemeObject, schemeDesigner, view) {
            let context = view.getContext();
            let objectParams = schemeObject.getParams();
            let backgroundColor = '#' + objectParams.backgroundColor;

            context.beginPath();
            context.lineWidth = 4;
            context.strokeStyle = 'white';

            let isHovered = schemeObject.isHovered && !SchemeDesigner.Tools.touchSupported();

            context.fillStyle = backgroundColor;

            if (objectParams.isSelected && isHovered) {
                context.strokeStyle = backgroundColor;
            } else if (isHovered) {
                context.fillStyle = 'white';
                context.strokeStyle = backgroundColor;
            } else if (objectParams.isSelected) {
                context.strokeStyle = backgroundColor;
            }

            let relativeX = schemeObject.x;
            let relativeY = schemeObject.y;

            let width = schemeObject.getWidth();
            let height = schemeObject.getHeight();
            if (!isHovered && !objectParams.isSelected) {
                let borderOffset = 4;
                relativeX = relativeX + borderOffset;
                relativeY = relativeY + borderOffset;
                width = width - (borderOffset * 2);
                height = height - (borderOffset * 2);
            }

            let halfWidth = width / 2;
            let halfHeight = height / 2;

            let circleCenterX = relativeX + halfWidth;
            let circleCenterY = relativeY + halfHeight;

            if (schemeObject.getRotation()) {
                context.save();
                context.translate( relativeX + halfWidth, relativeY + halfHeight);
                context.rotate(schemeObject.getRotation() * Math.PI / 180);
                context.rect(-halfWidth, -halfHeight, width, height);
            } else {
                context.arc(circleCenterX, circleCenterY, halfWidth, 0, Math.PI * 2);
            }

            context.fill();
            context.stroke();
            context.font = (Math.floor((schemeObject.getWidth() + schemeObject.getHeight()) / 4)) + 'px Arial';

            if (objectParams.isSelected && isHovered) {
                context.fillStyle = 'white';
            } else if (isHovered) {
                context.fillStyle = backgroundColor;
            } else if (objectParams.isSelected) {
                context.fillStyle = 'white';
            }

            if (objectParams.isSelected || isHovered) {
                context.textAlign = 'center';
                context.textBaseline = 'middle';
                if (schemeObject.rotation) {
                    context.fillText(objectParams.seat,
                        -(schemeObject.getWidth() / 2) + (schemeObject.getWidth() / 2),
                        -(schemeObject.getHeight() / 2)  + (schemeObject.getHeight() / 2)
                    );
                } else {
                    context.fillText(objectParams.seat, relativeX + (schemeObject.getWidth() / 2), relativeY + (schemeObject.getHeight() / 2));
                }
            }

            if (schemeObject.rotation) {
                context.restore();
            }
        };

        /**
         * Render label function
         * @param {SchemeObject} schemeObject
         * @param {Scheme} schemeDesigner
         * @param {View} view
         */
        let renderLabel = function(schemeObject, schemeDesigner, view) {
            let objectParams = schemeObject.getParams();
            let context = view.getContext();

            context.fillStyle = '#' + objectParams.fontColor;
            context.textAlign = 'center';
            context.textBaseline = 'middle';
        };

        let clickOnPlace = function (schemeObject, schemeDesigner, view, e)
        {
            let objectParams = schemeObject.getParams();
            objectParams.isSelected = !objectParams.isSelected;
        };

        for (let i = 0; i < schemeData.length; i++)
        {
            let objectData = schemeData[i];
            let leftOffset = objectData.CX >> 0;
            let topOffset = objectData.CY >> 0;
            let width = objectData.Width >> 0;
            let height = objectData.Height >> 0;
            let rotation = objectData.Angle >> 0;
            let schemeObject = new SchemeDesigner.SchemeObject({
                x: 0.5 + leftOffset,
                y: 0.5 + topOffset,
                width: width,
                height: height,
                active: objectData.ObjectType == 'Place' && objectData.Available ? true : false,
                renderFunction: objectData.ObjectType == 'Place' ? renderPlace : renderLabel,
                cursorStyle: objectData.ObjectType == 'Place' ? 'pointer' : 'default',
                rotation: rotation,
                id: 'place_' + i,
                seat: objectData.Seat,
                backgroundColor: objectData.Available ? objectData.BackColor : 'FFF',
                isSelected: false,
                clickFunction: clickOnPlace,
                clearFunction: function (schemeObject, schemeDesigner, view) {
                    let context = view.getContext();
                    let borderWidth = 5;
                    context.clearRect(schemeObject.x - borderWidth,
                        schemeObject.y - borderWidth,
                        this.width + (borderWidth * 2),
                        this.height + (borderWidth * 2)
                    );
                }
            });

            defaultLayer.addObject(schemeObject);
        }

        let canvas = document.getElementById('canvas1');
        let schemeDesigner = new SchemeDesigner.Scheme(canvas, {
            options: {
                cacheSchemeRatio: 2
            },
            scroll: {
                maxHiddenPart: 0.5
            },
            zoom: {
                padding: 0.1,
                maxScale: 8,
                zoomCoefficient: 1.04
            },
            storage: {
                treeDepth: 6
            }
        });

        schemeDesigner.addLayer(defaultLayer);
        schemeDesigner.render();

        function addPlaceField(value) {
            let x = document.getElementById("reservePlaceForm");
            let elem = x.querySelector(`input[id=places_${value}]`);
            if (elem) {
                elem.remove();
                return;
            }

            let new_field = document.createElement("input");
            new_field.setAttribute("type", "hidden");
            new_field.setAttribute("id", `places_${value}`);
            new_field.setAttribute("name", `places[${value}]`);
            new_field.value = value;
            let pos = x.childElementCount;
            x.insertBefore(new_field, x.childNodes[pos]);
        }

        canvas.addEventListener('schemeDesigner.clickOnObject', function (e) {
            addPlaceField(e.detail.id);
        }, false);
    </script>
</x-layout>
