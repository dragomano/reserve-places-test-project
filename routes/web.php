<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ShowsController@index')
    ->name('shows');

Route::get('/shows/{id}/events', 'ShowsController@events')
    ->whereNumber('id')
    ->name('events');

Route::get('/events/{id}/places', 'EventsController@index')
    ->whereNumber('id')
    ->name('places');

Route::post('/events/{id}/reserve', 'EventsController@reserve')
    ->whereNumber('id')
    ->name('reserve');
