<?php

namespace Tests\Feature;

use Faker\Factory;
use App\Http\Requests\ReservePlaceRequest;
use Illuminate\Validation\Validator;
use Tests\TestCase;

class ReservePlaceRequestTest extends TestCase
{
    /** @var ReservePlaceRequest */
    private $rules;

    /** @var Validator */
    private $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->validator = app()->get('validator');
        $this->rules = (new ReservePlaceRequest())->rules();
    }

    public function validationProvider(): array
    {
        $faker = Factory::create(Factory::DEFAULT_LOCALE);

        return [
            'request_should_fail_when_no_name_is_provided' => [
                'passed' => false,
                'data' => [
                    'places' => [$faker->numberBetween(1, 50)]
                ]
            ],
            'request_should_fail_when_no_places_is_provided' => [
                'passed' => false,
                'data' => [
                    'name' => $faker->name()
                ]
            ],
            'request_should_fail_when_name_has_more_than_50_characters' => [
                'passed' => false,
                'data' => [
                    'name' => $faker->text(51)
                ]
            ],
            'request_should_pass_when_data_is_provided' => [
                'passed' => true,
                'data' => [
                    'name' => $faker->name(),
                    'places' => [$faker->numberBetween(1, 50)]
                ]
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validationProvider
     * @param bool $shouldPass
     * @param array $mockedRequestData
     */
    public function validation_results_as_expected(bool $shouldPass, array $mockedRequestData)
    {
        $this->assertEquals(
            $shouldPass,
            $this->validate($mockedRequestData)
        );
    }

    protected function validate($mockedRequestData)
    {
        return $this->validator
            ->make($mockedRequestData, $this->rules)
            ->passes();
    }
}
