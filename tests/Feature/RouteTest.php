<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RouteTest extends TestCase
{
    use WithFaker;

    /**
     * @return void
     */
    public function test_route_shows_should_be_available()
    {
        $response = $this->get(route('shows'));

        $response->assertStatus(200)
            ->assertSee('Shows');
    }

    /**
     * @return void
     */
    public function test_route_events_should_be_available()
    {
        $response = $this->get(route('events', 1));

        $response->assertStatus(200)
            ->assertSee('Show #1 - Events');
    }

    /**
     * @return void
     */
    public function test_route_places_should_be_available()
    {
        $response = $this->get(route('places', 1));

        $response->assertStatus(200)
            ->assertSee('Choose a place what you want');
    }

    /**
     * @return void
     */
    public function test_route_reserve_should_be_available()
    {
        $response = $this->post(route('reserve', 1), [
            'name' => $this->faker()->name()
        ]);

        $response->assertStatus(302);
    }
}
